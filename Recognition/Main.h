#pragma once

#include <opencv/cv.h>
#include <opencv/highgui.h>

namespace Recognition {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Collections::Generic;
	using namespace System::Drawing;
	using namespace System::Text;
	using namespace System::Threading::Tasks;
	using namespace System::IO;
	using namespace cv;
	//using namespace std;
	
	// Distribution of pixels for each number
	double numbers[][10] = { 
				{0.25, 0.25, 0.25, 0.25},
				{0.2, 0.5, 0.3, 0},
				{0.2, 0.3, 0.19, 0.31},
				{0.15, 0.35, 0.35, 0.15},
				{0.33, 0.33, 0.33, 0},
				{0.36, 0.15, 0.35, 0.14},
				{0.29, 0.12, 0.29, 0.3},
				{0.19, 0.49, 0.02, 0.3},
				{0.25, 0.25, 0.25, 0.25},
				{0.29, 0.3, 0.29, 0.12}
			};

	/// <summary>
	/// ������ ��� Main
	/// </summary>
	public ref class Main : public System::Windows::Forms::Form
	{
	public:
		// Is on draw now or not
        bool on_draw;
		// Is on capture now or not
        bool on_capture;
		// Previous mouse location
        int prev_x;
        int prev_y;
	private: System::Windows::Forms::TextBox^  result_txtbox;
	private: System::Windows::Forms::CheckBox^  camera_checkbox;
	public: 
		// Thickness of pen
        int thickness;
		VideoCapture* capture;
	private: System::Windows::Forms::Timer^  timer;
	public: 
		IplImage* frame;

		Main(void){
			on_draw = false;
            on_capture = false;
            thickness = 3;

			frame = 0;

			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Main()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pic_box;
	private: System::Windows::Forms::Button^  clear_btn;
	private: System::Windows::Forms::VScrollBar^  thickness_scroll_bar;
	private: System::Windows::Forms::Button^  define_button;
	private: System::ComponentModel::IContainer^  components;


	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->pic_box = (gcnew System::Windows::Forms::PictureBox());
			this->clear_btn = (gcnew System::Windows::Forms::Button());
			this->thickness_scroll_bar = (gcnew System::Windows::Forms::VScrollBar());
			this->define_button = (gcnew System::Windows::Forms::Button());
			this->result_txtbox = (gcnew System::Windows::Forms::TextBox());
			this->camera_checkbox = (gcnew System::Windows::Forms::CheckBox());
			this->timer = (gcnew System::Windows::Forms::Timer(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pic_box))->BeginInit();
			this->SuspendLayout();
			// 
			// pic_box
			// 
			this->pic_box->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pic_box->Location = System::Drawing::Point(12, 12);
			this->pic_box->Name = L"pic_box";
			this->pic_box->Size = System::Drawing::Size(458, 420);
			this->pic_box->TabIndex = 0;
			this->pic_box->TabStop = false;
			this->pic_box->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &Main::pic_box_MouseDown);
			this->pic_box->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &Main::pic_box_MouseMove);
			this->pic_box->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &Main::pic_box_MouseUp);
			// 
			// clear_btn
			// 
			this->clear_btn->Location = System::Drawing::Point(476, 405);
			this->clear_btn->Name = L"clear_btn";
			this->clear_btn->Size = System::Drawing::Size(107, 27);
			this->clear_btn->TabIndex = 1;
			this->clear_btn->Text = L"Clear";
			this->clear_btn->UseVisualStyleBackColor = true;
			this->clear_btn->Click += gcnew System::EventHandler(this, &Main::clear_btn_Click);
			// 
			// thickness_scroll_bar
			// 
			this->thickness_scroll_bar->LargeChange = 1;
			this->thickness_scroll_bar->Location = System::Drawing::Point(476, 231);
			this->thickness_scroll_bar->Maximum = 15;
			this->thickness_scroll_bar->Minimum = 3;
			this->thickness_scroll_bar->Name = L"thickness_scroll_bar";
			this->thickness_scroll_bar->Size = System::Drawing::Size(107, 171);
			this->thickness_scroll_bar->TabIndex = 3;
			this->thickness_scroll_bar->Value = 3;
			this->thickness_scroll_bar->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &Main::thickness_scroll_bar_Scroll);
			// 
			// define_button
			// 
			this->define_button->Location = System::Drawing::Point(476, 201);
			this->define_button->Name = L"define_button";
			this->define_button->Size = System::Drawing::Size(107, 27);
			this->define_button->TabIndex = 4;
			this->define_button->Text = L"Define";
			this->define_button->UseVisualStyleBackColor = true;
			this->define_button->Click += gcnew System::EventHandler(this, &Main::define_button_Click);
			// 
			// result_txtbox
			// 
			this->result_txtbox->Location = System::Drawing::Point(476, 175);
			this->result_txtbox->Name = L"result_txtbox";
			this->result_txtbox->Size = System::Drawing::Size(107, 20);
			this->result_txtbox->TabIndex = 5;
			// 
			// camera_checkbox
			// 
			this->camera_checkbox->AutoSize = true;
			this->camera_checkbox->Location = System::Drawing::Point(476, 12);
			this->camera_checkbox->Name = L"camera_checkbox";
			this->camera_checkbox->Size = System::Drawing::Size(83, 17);
			this->camera_checkbox->TabIndex = 6;
			this->camera_checkbox->Text = L"Use camera";
			this->camera_checkbox->UseVisualStyleBackColor = true;
			this->camera_checkbox->CheckedChanged += gcnew System::EventHandler(this, &Main::camera_checkbox_CheckedChanged);
			// 
			// timer
			// 
			this->timer->Tick += gcnew System::EventHandler(this, &Main::timer_Tick);
			// 
			// Main
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(592, 444);
			this->Controls->Add(this->camera_checkbox);
			this->Controls->Add(this->result_txtbox);
			this->Controls->Add(this->define_button);
			this->Controls->Add(this->thickness_scroll_bar);
			this->Controls->Add(this->clear_btn);
			this->Controls->Add(this->pic_box);
			this->Name = L"Main";
			this->Text = L"Main";
			this->Load += gcnew System::EventHandler(this, &Main::Main_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pic_box))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		
	// Clear picture box area
	private: System::Void clear_pic_box(){
		Bitmap^ bmp = gcnew Bitmap(pic_box->Width, pic_box->Height);
		Graphics^ g = Graphics::FromImage(bmp);
		g->Clear(Color::White);
        pic_box->Image = bmp;
		pic_box->Refresh();
	}

	// On window load initialize a picture box with white bitmap
	private: System::Void Main_Load(System::Object^  sender, System::EventArgs^  e) {
		Bitmap^ bmp = gcnew Bitmap(pic_box->Width, pic_box->Height);
		Graphics^ g = Graphics::FromImage(bmp);
		g->Clear(Color::White);
        pic_box->Image = bmp;
		pic_box->Refresh();
	}

	// On mouse down change on draw state to true and remember position of mouse
	private: System::Void pic_box_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		on_draw = true;
		prev_x = e->X;
		prev_y = e->Y;
	}

	// On mouse move check if on draw = true and if yes - draw line from previous mouse position to now prosition
	private: System::Void pic_box_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		    if(on_draw){
                if (pic_box->Image == nullptr)
                {
                    clear_pic_box();
                }
				Graphics^ g = Graphics::FromImage(pic_box->Image);

				// We have a problem with painting: artefacts of dashed line
                //g->DrawRectangle(gcnew Pen(Brushes::Black, thickness), prev_x, prev_y, thickness, thickness);
                g->DrawEllipse(gcnew Pen(Brushes::Black, thickness), prev_x, prev_y, thickness, thickness);
				//g->DrawLine(gcnew Pen(Brushes::Black, thickness), prev_x, prev_y, e->X, e->Y);

                pic_box->Invalidate();
                prev_x = e->X;
                prev_y = e->Y;
		    }
	}

	// On mouse up just change on draw state to false
	private: System::Void pic_box_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		on_draw = false;
	}

	// Reinitialize picture box with white bitmap
	private: System::Void clear_btn_Click(System::Object^  sender, System::EventArgs^  e) {
		clear_pic_box();
	}

	// On scroll bar value changed we redefine thickness of pen
	private: System::Void thickness_scroll_bar_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e) {
		thickness = thickness_scroll_bar->Value;
	}

	private: int which(double a0, double a1, double a2, double a3){
        int m = 0;
        double* t = new double[10];
        double s = 10;
        for (int i = 0; i < 10; ++i)
        {
            double p0 = (numbers[i][0] - a0) * (numbers[i][0] - a0);
            double p1 = (numbers[i][1] - a1) * (numbers[i][1] - a1);
            double p2 = (numbers[i][2] - a2) * (numbers[i][2] - a2);
            double p3 = (numbers[i][3] - a3) * (numbers[i][3] - a3);
            t[i] = (double)System::Math::Sqrt((p0 + p1 + p2 + p3));
            if (t[i] < s) m = i;
        }
        return m;
    }

	private: System::Void Process(array<int,2>^ M, int x, int y, int width, int height, int fill)
    {
        while (true)
        {
            bool i = false;
            // 4 - ���������
            if (x + 1 < width && !i)
            {
                if (M[x + 1, y] == 0)
                {
                    x = x + 1;
                    i = true;
                }
            }
            if (y + 1 < height && !i)
            {
                if (M[x, y + 1] == 0)
                {
                    y = y + 1;
                    i = true;
                }
            }
            if (x - 1 >= 0 && !i)
            {
                if (M[x - 1, y] == 0)
                {
                    x = x - 1;
                    i = true;
                }
            }
            if (y - 1 >= 0 && !i)
            {
                if (M[x, y - 1] == 0)
                {
                    y = y - 1;
                    i = true;
                }
            }
            if (!i) return;
            M[x, y] = fill;
        }
    }

	private: System::Void define_button_Click(System::Object^  sender, System::EventArgs^  e) {
		int b_x = 10000, b_y = 10000, e_x = -1, e_y = -1;
            Bitmap^ image = (Bitmap^)pic_box->Image;

			array<bool, 2>^ matrix = gcnew array<bool, 2>(image->Width, image->Height); 
			array<int, 2>^ M = gcnew array<int, 2>(image->Width, image->Height); 

            for (int y = 0; y < image->Height; y++)
            {
                for (int x = 0; x < image->Width; x++)
                {
                    Color c = image->GetPixel(x, y);
                    if (c.R < 100 && c.G < 100 && c.B < 100)
                    {
						if (x < b_x)
							b_x = x;
						if (y < b_y)
							b_y = y;
						if (x > e_x)
							e_x = x;
						if (y > e_y)
							e_y = y;

                        matrix[x, y] = true;
                        M[x, y] = 1;
                    }else{
                        matrix[x, y] = false;
                        M[x, y] = 0;
                    }
                }
            }
            int dx = e_x - b_x;
            int dy = e_y - b_y;

            int left_top = 0, right_top = 0, left_bottom = 0, right_bottom = 0;

            // 1
            for (int x = b_x; x < b_x + dx / 2; ++x)
            {
                for (int y = b_y; y < b_y + dy / 2; ++y)
                {
                    if (matrix[x, y])
                        ++left_top;
                }
            }
            // 2
            for (int x = b_x; x < b_x + dx / 2; ++x)
            {
                for (int y = b_y + dy / 2; y < e_y; ++y)
                {
                    if (matrix[x, y])
                        ++right_top;
                }
            }
            // 3
            for (int x = b_x + dx / 2; x < e_x; ++x)
            {
                for (int y = b_y + dy / 2; y < e_y; ++y)
                {
                    if (matrix[x, y])
                        ++right_bottom;
                }
            }
            // 4
            for (int x = b_x + dx / 2; x < e_x; ++x)
            {
                for (int y = b_y; y < b_y + dy / 2; ++y)
                {
                    if (matrix[x, y])
                        ++left_bottom;
                }
            }

            int all = left_top + left_bottom + right_top + right_bottom;
            double a0 = (double)left_top / (double)all;
            double a1 = (double)right_top / (double)all;
            double a2 = (double)right_bottom / (double)all;
            double a3 = (double)left_bottom / (double)all;
            result_txtbox->Text = which(a0, a1, a2, a3).ToString();

            

            int fill = 2;
            for (int y = 0; y < image->Height; y++)
            {
                for (int x = 0; x < image->Width; x++)
                {
                    if (M[x, y] == 0)
                        Process(M, x, y, image->Width, image->Height, fill++);
                }
            }

			result_txtbox->Text = Convert::ToString(fill);
	}	 

	private: System::Void camera_checkbox_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		if (camera_checkbox->Checked)
        {
            if (capture == nullptr)
            {
                try
                {
					capture = new VideoCapture(0);
                }
				catch (NullReferenceException^ excpt)
                {
                    MessageBox::Show(excpt->Message);
                }
            }

			// Get FPS from video file
				int fps = (int)capture->get(CV_CAP_PROP_FPS);

				// Asssign user defined value to fps in case it equals to 0
				if(fps == 0)
					fps = 24;

				// Set timer interval value in milliseconds
				timer->Interval = 1000/fps;

				// Start timer
				timer->Start();
        }else{
			capture = nullptr;
			timer->Stop();
		}
	}

	private: System::Void timer_Tick(System::Object^  sender, System::EventArgs^  e) {
		// Frame matrix
		cv::Mat frame;

		// Get next frame from capture object
		*capture >> frame;

		// Stop palaying when there are no more frames
		if( frame.empty() )
		{
			timer->Stop();
			pic_box->Image = nullptr;
			return;
		}

		// Convert OpenCV Mat object to the Image object of .Net
		Image ^img = gcnew Bitmap(frame.cols, frame.rows, frame.step,
			System::Drawing::Imaging::PixelFormat::Format24bppRgb, IntPtr(frame.data));

		// Release frame
		frame.release();

		// Copy image to the PictureBox control
		pic_box->Image = img;
	}
};
}

